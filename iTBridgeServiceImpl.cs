﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Google.Protobuf.Collections;
using Grpc.Core;
using Itbridge;
using iTunesLib;

namespace itbridge {
    class iTBridgeServiceImpl : iTBridgeService.iTBridgeServiceBase {
        // Server side handler of the SayHello RPC
        public override Task<HelloReply> Hello(PayloadEmpty request, ServerCallContext context) {
            return Task.FromResult(new HelloReply {
                AppName = Assembly.GetExecutingAssembly().GetName().Name,
                AppVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString()
            });
        }

        public override Task<TracksList> TracksGetSelected(PayloadEmpty request, ServerCallContext context) {
            iTunesApp iTunesApp = iTunesManager.Instance.GetItunesHandler();
            if (iTunesApp.SelectedTracks == null) return Task.FromResult(new TracksList() { Quantity = 0 });

            // retrieve and return tracks
            return Task.FromResult(getTracksListFromTracksCollection(iTunesApp.SelectedTracks));
        }

        public override async Task TracksStreamSelected(PayloadEmpty request,
            IServerStreamWriter<TracksListStream> responseStream, ServerCallContext context) {
            iTunesApp iTunesApp = iTunesManager.Instance.GetItunesHandler();
            if (iTunesApp.SelectedTracks == null) return;

            int i = 0;
            int total = iTunesApp.SelectedTracks.Count;
            ArrayList selectedTracks = new ArrayList();

            // save the currently selected tracks
            foreach (IITTrack selectedTrack in iTunesApp.SelectedTracks) {
                selectedTracks.Add(selectedTrack);
            }

            // send them to client
            foreach (IITTrack selectedTrack in selectedTracks) {
                if (context.CancellationToken.IsCancellationRequested) break;

                iTObjectId trackId = new iTObjectId();
                trackId.IdDatabase = selectedTrack.TrackDatabaseID;
                trackId.IdPlaylist = selectedTrack.playlistID;
                trackId.IdSource = selectedTrack.sourceID;
                trackId.IdTrack = selectedTrack.trackID;

                Track track = new Track {
                    Id = trackId,
                    Title = selectedTrack.Name,
                    Album = selectedTrack.Album,
                    Artist = selectedTrack.Artist,
                };

                await responseStream.WriteAsync(new TracksListStream { Current = i, Total = total, Track = track });
                i++;
            }
        }

        public override Task<PayloadOperationResult> TrackSetLyrics(TrackIdAndLyrics trackIdAndLyrics,
            ServerCallContext context) {
            iTunesApp iTunesApp = iTunesManager.Instance.GetItunesHandler();

            IITObject obj = iTunesApp.GetITObjectByID(
                trackIdAndLyrics.Id.IdSource,
                trackIdAndLyrics.Id.IdPlaylist,
                trackIdAndLyrics.Id.IdTrack,
                trackIdAndLyrics.Id.IdDatabase);

            // try to cast
            IITFileOrCDTrack track = itObjectToFileOrCDTrack(obj);
            if (track == null)
                return Task.FromResult(new PayloadOperationResult { Ok = false, Message = "", ResultId = 0 });

            // set lyrics
            track.Lyrics = trackIdAndLyrics.Lyrics;
            return Task.FromResult(new PayloadOperationResult { Ok = true, Message = "", ResultId = 0 });
        }

        public override Task<PayloadBoolean> TrackGetHasLyrics(iTObjectId trackId, ServerCallContext context) {
            iTunesApp iTunesApp = iTunesManager.Instance.GetItunesHandler();

            IITObject obj = iTunesApp.GetITObjectByID(
                trackId.IdSource,
                trackId.IdPlaylist,
                trackId.IdTrack,
                trackId.IdDatabase);

            // try to cast
            IITFileOrCDTrack track = itObjectToFileOrCDTrack(obj);
            if (null == track) return Task.FromResult(new PayloadBoolean { Content = false });

            return Task.FromResult(new PayloadBoolean { Content = !string.IsNullOrEmpty(track.Lyrics) });
        }

        public override Task<PlaylistsList> PlaylistsGet(PayloadEmpty request, ServerCallContext context) {
            iTunesApp iTunesApp = iTunesManager.Instance.GetItunesHandler();

            PlaylistsList outPlaylistsList = new PlaylistsList();

            foreach (IITPlaylist libraryPlaylist in iTunesApp.LibrarySource.Playlists) {
                iTObjectId playlistId = new iTObjectId();
                playlistId.IdPlaylist = libraryPlaylist.playlistID;
                playlistId.IdDatabase = libraryPlaylist.TrackDatabaseID;
                playlistId.IdTrack = libraryPlaylist.trackID;
                playlistId.IdSource = libraryPlaylist.sourceID;

                Playlist playlist = new Playlist();
                playlist.Name = libraryPlaylist.Name;
                playlist.Id = playlistId;

                outPlaylistsList.Playlists.Add(playlist);
            }

            outPlaylistsList.Quantity = iTunesApp.LibrarySource.Playlists.Count;

            return Task.FromResult(outPlaylistsList);
        }

        public override Task<TracksList> PlaylistGetTracks(iTObjectId request, ServerCallContext context) {
            iTunesApp iTunesApp = iTunesManager.Instance.GetItunesHandler();

            // retrieve the playlist object
            IITObject playlistObject = iTunesApp.GetITObjectByID(request.IdSource, request.IdPlaylist, request.IdTrack,
                request.IdDatabase);

            // try to cast
            if (!(playlistObject is IITPlaylist)) return Task.FromResult(new TracksList { Quantity = 0 });

            // retrieve tracks
            IITPlaylist playlist = (IITPlaylist)playlistObject;
            TracksList trackList = getTracksListFromTracksCollection(playlist.Tracks);
            return Task.FromResult(trackList);
        }

        /*
         *  Utils
         */

        private IITFileOrCDTrack itObjectToFileOrCDTrack(IITObject obj) {
            // try to cast #1
            if (!(obj is IITTrack))
                return null;

            IITTrack track = (IITTrack)obj;

            // try to cast #2
            if (!(track is IITFileOrCDTrack))
                return null;

            return (IITFileOrCDTrack)track;
        }

        private TracksList getTracksListFromTracksCollection(IITTrackCollection tracksCollection) {
            var toSend = new TracksList();

            // save the currently selected tracks
            foreach (IITTrack selectedTrack in tracksCollection) {
                iTObjectId toAddTrackId = new iTObjectId();
                toAddTrackId.IdDatabase = selectedTrack.TrackDatabaseID;
                toAddTrackId.IdPlaylist = selectedTrack.playlistID;
                toAddTrackId.IdSource = selectedTrack.sourceID;
                toAddTrackId.IdTrack = selectedTrack.trackID;

                Track toAddTrack = new Track();
                toAddTrack.Id = toAddTrackId;
                toAddTrack.Title = selectedTrack.Name;
                toAddTrack.Album = selectedTrack.Album;
                toAddTrack.Artist = selectedTrack.Artist;
                toSend.Tracks.Add(toAddTrack);
            }

            toSend.Quantity = toSend.Tracks.Count;
            return toSend;
        }
    }
}