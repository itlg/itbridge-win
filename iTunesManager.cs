﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTunesLib;

namespace itbridge {

    public sealed class iTunesManager {
        private iTunesApp miTunesApp = null;
        private static readonly Lazy<iTunesManager> lazy = new Lazy<iTunesManager>(() => new iTunesManager());

        public static iTunesManager Instance { get { return lazy.Value; } }

        private iTunesManager() {
            miTunesApp = new iTunesApp();
        }

        public iTunesApp GetItunesHandler() {
            return miTunesApp;
        }
    }
}