﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using itbridge;
using Itbridge;
using iTunesLib;

namespace itbridge {
    class Program {
        const int Port = 50051;

        public static void Main(string[] args) {
            iTunesApp oItunes = iTunesManager.Instance.GetItunesHandler();

            Server server = new Server {
                Services = { iTBridgeService.BindService(new iTBridgeServiceImpl()) },
                Ports = { new ServerPort("localhost", Port, ServerCredentials.Insecure) }
            };
            server.Start();

            server.ShutdownTask.Wait();

            // Console.WriteLine("Greeter server listening on port " + Port);
            // Console.WriteLine("Press any key to stop the server...");
            // Console.ReadKey();

            // server.ShutdownAsync().Wait();
        }
    }
}